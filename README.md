# ToDo App - built with Django

A very simple ToDo app, built with Django.

## Initial Project Setup

```shell
# *ix shell only
git clone git@gitlab.com:augensalat/todo-django.git
cd todo-django
make install

# optionally
. .venv/bin/activate
pip install --upgrade pip
```

## Run the development server

```shell
make server
```
